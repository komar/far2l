Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: far2l
Upstream-Contact: elfmz
Files-Excluded: multiarc/src/formats/rar
Source: https://github.com/elfmz/far2l

Files: *
Copyright: 2016-2023 elfmz
           2016-2021 anatoly techtonik
           2016-2016 Igor Ingultsov
           2016-2016 lieff
           2016-2017 volth
           2016-2016 Gravityzwell
           2016-2016 Igor Klopov
           2016-2016 Eugene Sandulenko
           2017-2017 Danila Sukharev
           2017-2017 John Doe
           2017-2018 MikeMirzayanov
           2018-2018 Thinking Errol
           2018-2018 Sergey Kosarevsky
           2018-2018 eeprom
           2018-2018 Artiom
           2018-2018 Ivan Vasyliev
           2018-2022 Victor Sergienko
           2018-2020 Randolf Richardson
           2018-2019 svost
           2018-2021 Yurii Kolesnykov
           2018-2018 Yevgeniy Filatov
           2019-2019 Stanislav Mekhanoshin
           2019-2019 russiandesman
           2019-2019 Alter-1
           2019-2019 ZOleg73
           2019-2019 Denis Adamchuk
           2019-2019 kas
           2019-2019 colier
           2019-2019 Ilia Konnov
           2019-2019 ha3flt
           2019-2019 vodek3
           2020-2022 m32
           2020-2020 Dmitry Lomanov
           2020-2020 Dmitry Stogov
           2020-2020 Anton Argirov
           2020-2022 Ivan Sorokin
           2020-2020 developerxyzw
           2020-2020 Artem Vetrov
           2020-2020 Alex Yaroslavsky
           2020-2021 Alexei Golovin
           2021-2021 hypersw
           2021-2021 Mikhail Lukashov
           2021-2021 sxored
           2021-2021 Alexandr Kozlinskiy
           2021-2021 Anton Samokhvalov
           2021-2021 Alexey Nosov
           2021-2021 Andrew Clarke
           2021-2021 bolddwarf
           2021-2021 Murad Kakabaev
           2021-2021 vik
           2022-2022 Pavel Dovgalyuk
           2022-2022 Ivan Shatsky
License: GPL-2

Files: colorer/*
Copyright: 1999-2009 Igor Ruskih
           2009-2020 Aleksey Dobrunov
           2009-2020 Eugene Efremov
           2009-2020 Anatoly Techtonik
           2009-2020 2useven10
           2009-2020 Max Galkin
           2016-2022 elfmz
           2016-2016 Igor Ingultsov
           2016-2016 lieff
           2016-2016 volth
           2018-2018 eeprom
           2018-2019 svost
           2020-2020 Anton Argirov
           2020-2020 Victor Sergienko
           2020-2020 Ivan Sorokin
           2021-2021 Alexei Golovin
           2021-2021 Anton Samokhvalov
           2021-2021 Mikhail Lukashov
License: BSD-3-clause

Files: colorer/src/Colorer-library/src/colorer/unicode/x_tables_generator.pl colorer/src/Colorer-library/src/colorer/unicode/x_encodings.pl colorer/src/pcolorer2/*
Copyright: 1999-2009 Igor Ruskih
           2009-2020 Aleksey Dobrunov
           2009-2020 Eugene Efremov
           2009-2020 Anatoly Techtonik
           2009-2020 2useven10
           2009-2020 Max Galkin
           2016-2022 elfmz
           2016-2016 lieff
           2016-2016 Igor Ingultsov
           2016-2016 volth
           2018-2018 eeprom
           2018-2019 svost
           2020-2020 Ivan Sorokin
           2021-2021 Anton Samokhvalov
License: GPL-2

Files: farlng/*
Copyright: 2003-2005 WARP ItSelf
           2005 Alex Yaroslavsky
           2016-2021 elfmz
           2016-2016 Igor Ingultsov
           2016-2016 lieff
           2018-2018 eeprom
           2019-2019 ZOleg73
           2020-2020 Victor Sergienko
License: BSD-3-clause

Files: compare/* drawline/* editcase/* align/* autowrap/* farftp/* FARStdlib/* filecase/* multiarc/* SimpleIndent/* tmppanel/*
Copyright: 1996-2000 Eugene Roshal
           2000-2021 Far Group
           2016-2022 elfmz
           2016-2016 Igor Ingultsov
           2016-2016 lieff
           2016-2016 volth
           2016-2016 Gravityzwell
           2016-2016 Eugene Sandulenko
           2018-2018 eeprom
           2018-2018 Victor Sergienko
           2018-2018 svost
           2020-2020 Ivan Sorokin
           2020-2020 Anton Argirov
           2021-2021 Mikhail Lukashov
           2022-2022 Pavel Dovgalyuk
License: BSD-3-clause

Files: calc/*
Copyright: 1998-2001 Igor Russkih
           2009-2012 uncle-vunkis
           2020-2020 FarPlugins Team
           2021-2022 elfmz
           2021-2021 Mikhail Lukashov
License: BSD-3-clause

Files: calc/src/shared/mathexpression/*
Copyright: 2009-2009 Andrey Myznikov
           2009-2009 Sergey Prokopenko
           2009-2009 Elizaveta Evstifeeva
           2021-2021 elfmz
License: BSD-3-clause

Files: calc/src/shared/ttmath/*
Copyright: 2006-2012 Tomasz Sowa
           2021-2021 elfmz
           2021-2021 Mikhail Lukashov
License: BSD-3-clause

Files: calc/src/shared/trex/*
Copyright: 2003-2006 Alberto Demichelis
           2021-2021 elfmz
License: T-Rex

Files: editorcomp/*
Copyright: 2017-2018 MikeMirzayanov
           2018-2022 elfmz
           2018-2018 svost
           2019-2019 Alter-1
License: GPL-2

Files: far2l/bootstrap/* far2l/src/panels/filelist.hpp far2l/src/cache.hpp far2l/src/exitcode.hpp far2l/src/dizlist.cpp far2l/src/plist.cpp far2l/src/panels/panel.hpp far2l/src/LICENSE.Far2.txt far2l/src/panels/flshow.cpp far2l/src/panels/infolist.cpp far2l/src/fileview.cpp far2l/src/editor.hpp far2l/src/filetype.hpp far2l/src/hist/history.cpp far2l/src/DlgGuid.hpp far2l/src/message.hpp far2l/src/frame.cpp far2l/src/FilesSuggestor.cpp far2l/src/dizlist.hpp far2l/src/DialogBuilder.cpp far2l/src/DialogBuilder.hpp far2l/src/fileattr.hpp far2l/src/dialog.hpp far2l/src/plist.hpp far2l/src/panels/filelist.cpp far2l/src/RefreshFrameManager.cpp far2l/src/copy.hpp far2l/src/fileowner.cpp far2l/src/panels/flupdate.cpp far2l/src/setattr.cpp far2l/src/frame.hpp far2l/src/filetype.cpp far2l/src/RefreshFrameManager.hpp far2l/src/panels/qview.cpp far2l/src/plug/plugins.cpp far2l/src/plug/plugapi.cpp far2l/src/plug/PluginW.hpp far2l/src/plug/PluginA.cpp far2l/src/plug/plugins.hpp far2l/src/plug/wrap.cpp far2l/src/plug/PluginA.hpp far2l/src/plug/plclass.hpp far2l/src/plug/PluginW.cpp far2l/src/plug/plclass.cpp far2l/src/plug/plugapi.hpp far2l/src/foldtree.hpp far2l/src/headers.hpp far2l/src/TPreRedrawFunc.cpp far2l/src/lang.hpp far2l/src/locale/DetectCodepage.h far2l/src/locale/locale.cpp far2l/src/locale/locale.hpp far2l/src/locale/xlat.hpp far2l/src/locale/codepage.hpp far2l/src/locale/DetectCodepage.cpp far2l/src/locale/xlat.cpp far2l/src/locale/codepage.cpp far2l/src/vmenu.hpp far2l/src/viewer.cpp far2l/src/fnparce.cpp far2l/src/stddlg.cpp far2l/src/base/SafeMMap.cpp far2l/src/setcolor.cpp far2l/src/base/farqueue.hpp far2l/src/global.cpp far2l/src/hilight.cpp far2l/src/stddlg.hpp far2l/src/global.hpp far2l/src/execute.cpp far2l/src/datetime.cpp far2l/src/rdrwdsk.cpp far2l/src/edit.cpp far2l/src/TPreRedrawFunc.hpp far2l/src/keybar.hpp far2l/src/execute.hpp far2l/src/panels/treelist.cpp far2l/src/findfile.cpp far2l/src/panels/panel.cpp far2l/src/bookmarks/BookmarksMenu.cpp far2l/src/bookmarks/BookmarksLegacy.cpp far2l/src/base/SafeMMap.hpp far2l/src/FilesSuggestor.hpp far2l/src/hist/poscache.hpp far2l/src/filefilterparams.cpp far2l/src/farwinapi.hpp far2l/src/base/farqueue.cpp far2l/src/synchro.cpp far2l/src/filepanels.cpp far2l/src/viewer.hpp far2l/src/rdrwdsk.hpp far2l/src/fileedit.hpp far2l/src/menubar.cpp far2l/src/dirinfo.cpp far2l/src/filestr.cpp far2l/src/filepanels.hpp far2l/src/options.hpp far2l/src/hist/history.hpp far2l/src/panels/infolist.hpp far2l/src/filefilter.cpp far2l/src/cmdline.cpp far2l/src/cfg/language.cpp far2l/src/panels/flmodes.cpp far2l/src/filefilter.hpp far2l/src/findfile.hpp far2l/src/hmenu.cpp far2l/src/vmenu.cpp far2l/src/scantree.cpp far2l/src/syslog.hpp far2l/src/cmdline.hpp far2l/src/cfg/language.hpp far2l/src/dirinfo.hpp far2l/src/modal.cpp far2l/src/foldtree.cpp far2l/src/datetime.hpp far2l/src/ctrlobj.hpp far2l/src/clipboard.cpp far2l/src/delete.hpp far2l/src/dlgedit.cpp far2l/src/ctrlobj.cpp far2l/src/modal.hpp far2l/src/execute_oscmd.cpp far2l/src/mkdir.cpp far2l/src/namelist.cpp far2l/src/mkdir.hpp far2l/src/help.cpp far2l/src/fileowner.hpp far2l/src/Mounts.cpp far2l/src/copy.cpp far2l/src/synchro.hpp far2l/src/hist/poscache.cpp far2l/src/setattr.hpp far2l/src/flink.hpp far2l/src/filestr.hpp far2l/src/console/scrsaver.cpp far2l/src/console/scrobj.cpp far2l/src/console/grabber.hpp far2l/src/console/constitle.hpp far2l/src/console/palette.cpp far2l/src/console/palette.hpp far2l/src/console/scrsaver.hpp far2l/src/console/grabber.cpp far2l/src/console/savescr.cpp far2l/src/console/lockscrn.cpp far2l/src/console/scrbuf.hpp far2l/src/console/lockscrn.hpp far2l/src/console/savescr.hpp far2l/src/console/scrbuf.cpp far2l/src/console/interf.cpp far2l/src/console/interf.hpp far2l/src/console/constitle.cpp far2l/src/console/console.hpp far2l/src/console/scrobj.hpp far2l/src/console/keys.hpp far2l/src/console/keyboard.cpp far2l/src/console/keyboard.hpp far2l/src/console/console.cpp far2l/src/console/colors.hpp far2l/src/menubar.hpp far2l/src/mix/cddrv.hpp far2l/src/mix/RegExp.hpp far2l/src/mix/pathmix.cpp far2l/src/mix/chgprior.hpp far2l/src/base/farrtl.hpp far2l/src/mix/mix.hpp far2l/src/mix/format.hpp far2l/src/mix/wakeful.hpp far2l/src/mix/panelmix.hpp far2l/src/mix/drivemix.hpp far2l/src/mix/drivemix.cpp far2l/src/base/bitflags.hpp far2l/src/mix/RegExp.cpp far2l/src/mix/panelmix.cpp far2l/src/base/array.hpp far2l/src/mix/mix.cpp far2l/src/mix/strmix.cpp far2l/src/mix/cvtname.hpp far2l/src/base/farrtl.cpp far2l/src/mix/strmix.hpp far2l/src/base/DList.hpp far2l/src/mix/pathmix.hpp far2l/src/base/FARString.cpp far2l/src/mix/processname.cpp far2l/src/mix/processname.hpp far2l/src/base/DList.cpp far2l/src/mix/cddrv.cpp far2l/src/base/CriticalSections.hpp far2l/src/base/FARString.hpp far2l/src/mix/dirmix.hpp far2l/src/mix/udlist.cpp far2l/src/base/noncopyable.hpp far2l/src/mix/dirmix.cpp far2l/src/mix/udlist.hpp far2l/src/mix/cvtname.cpp far2l/src/mix/chgprior.cpp far2l/src/mix/format.cpp far2l/src/cfg/config.cpp far2l/src/cfg/config.hpp far2l/src/cfg/HotkeyLetterDialog.hpp far2l/src/cfg/HotkeyLetterDialog.cpp far2l/src/usermenu.hpp far2l/src/syslog.cpp far2l/src/Mounts.hpp far2l/src/setcolor.hpp far2l/src/namelist.hpp far2l/src/farwinapi.cpp far2l/src/options.cpp far2l/src/fileview.hpp far2l/src/delete.cpp far2l/src/clipboard.hpp far2l/src/farversion.h far2l/src/dlgedit.hpp far2l/src/panelctype.hpp far2l/src/manager.cpp far2l/src/filefilterparams.hpp far2l/src/message.cpp far2l/src/fileedit.cpp far2l/src/filemask/FileMasksWithExclude.cpp far2l/src/filemask/CFileMask.cpp far2l/src/filemask/CFileMask.hpp far2l/src/filemask/FileMasksProcessor.hpp far2l/src/filemask/FileMasksWithExclude.hpp far2l/src/filemask/FileMasksProcessor.cpp far2l/src/filemask/BaseFileMask.hpp far2l/src/editor.cpp far2l/src/hmenu.hpp far2l/src/manager.hpp far2l/src/fileattr.cpp far2l/src/hilight.hpp far2l/src/edit.hpp far2l/src/scantree.hpp far2l/src/panels/treelist.hpp far2l/src/macro/TStack.hpp far2l/src/macro/tvar.hpp far2l/src/macro/chgmmode.cpp far2l/src/macro/macro.hpp far2l/src/macro/chgmmode.hpp far2l/src/macro/syntax.cpp far2l/src/macro/tvar.cpp far2l/src/macro/macro.cpp far2l/src/macro/macroopcode.hpp far2l/src/macro/syntax.hpp far2l/src/flink.cpp far2l/src/fnparce.hpp far2l/src/usermenu.cpp far2l/src/panels/qview.hpp far2l/src/main.cpp far2l/src/DlgGuid.cpp far2l/src/panels/flplugin.cpp far2l/src/keybar.cpp far2l/src/help.hpp far2l/src/dialog.cpp far2l/src/cache.cpp
Copyright: 1996-2000 Eugene Roshal
           2000-2021 Far Group
           2016-2022 elfmz
           2016-2016 Igor Ingultsov
           2016-2016 volth
           2018-2018 eeprom
           2019-2019 Alter-1
           2020-2020 Dmitry Stogov
           2020-2020 Anton Argirov
           2020-2020 Alexei Golovin
           2021-2021 sxored
           2021-2021 Alexandr Kozlinskiy
           2021-2021 bolddwarf
           2022-2022 Victor Sergienko
           2022-2022 Ivan Sorokin
           2022-2022 Ivan Shatsky
License: BSD-3-clause+FAR-Plugins-Exception

Files: far2l/src/vt/vtansi.cpp
Copyright: 2005-2014 Jason Hood
           2021-2021 elfmz
License: ANSICON

Files: multiarc/src/formats/ha/ha/*
Copyright: 1993-1993 Harri Hirvola
           2021-2022 elfmz
License: GPL-2

Files: multiarc/src/formats/7z/C/* multiarc/src/formats/7z/7zMain.c
Copyright: 2013-2019 Igor Pavlov
           2016-2020 elfmz
License: public-domain
 No copyright is claimed.  This code is in the public domain; do with
 it what you wish.

Files: NetRocks/*
Copyright: 2019-2022 elfmz
           2020-2020 Victor Sergienko
           2021-2021 Mikhail Lukashov
License: GPL-2+OpenSSL-Exception

Files: incsrch/*
Copyright: 2019-2019 Stanislav Mekhanoshin
           2019-2019 ZOleg73
           2019-2022 elfmz
           2021-2021 Mikhail Lukashov
License: GPL-2

Files: inside/*
Copyright: 2018-2022 elfmz
           2018-2018 svost
           2020-2020 Dmitry Lomanov
           2021-2021 Mikhail Lukashov
License: GPL-2

Files: packaging/*
Copyright: 2020-2020 Anton Argirov
           2020-2022 elfmz
           2020-2020 Alexei Golovin
           2021-2021 vik
License: GPL-2

Files: python/*
Copyright: 2020-2022 m32
           2020-2022 elfmz
           2020-2020 Anton Argirov
           2021-2021 anatoly techtonik
           2021-2021 Mikhail Lukashov
License: GPL-2

Files: utils/* WinPort/*
Copyright: 2016-2022 elfmz
           2016-2016 Igor Ingultsov
           2016-2016 lieff
           2017-2017 Danila Sukharev
           2017-2017 John Doe
           2018-2018 eeprom
           2018-2018 Sergey Kosarevsky
           2018-2018 Artiom
           2018-2021 Victor Sergienko
           2018-2019 svost
           2019-2019 Alter-1
           2019-2019 ZOleg73
           2019-2019 kas
           2020-2020 Dmitry Lomanov
           2020-2020 Dmitry Stogov
           2020-2020 Anton Argirov
           2021-2021 Mikhail Lukashov
           2022-2022 m32
           2022-2022 Ivan Sorokin
License: GPL-2

Files: utils/include/ww898/*
Copyright: 2017-2020 Mikhail Pilin
           2021-2021 elfmz
License: MIT

License: ANSICON
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the author be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 3. The name of the authors may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause+FAR-Plugins-Exception
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 3. The name of the authors may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 EXCEPTION:
 Far Manager plugins that use only the following header files from this
 distribution (none or any): farcolor.h, farkeys.h, farplug-wide.h, farplug-mb.h, fardlgbuilder.hpp,
 farcolorW.pas, farkeysW.pas and pluginW.pas; can be distributed under any other
 possible license with no implications from the above license on them.

License: GPL-2
 Please see /usr/share/common-licenses/GPL-2

License: GPL-2+OpenSSL-Exception
 Please see /usr/share/common-licenses/GPL-2
 -----------------------------------------------------------------------------
 .
 In addition, as a special exception, the copyright holders give
 permission to link the code of portions of this program with the
 OpenSSL library under certain conditions as described in each
 individual source file, and distribute linked combinations
 including the two.
 You must obey the GNU General Public License in all respects
 for all of the code used other than OpenSSL.  If you modify
 file(s) with this exception, you may extend this exception to your
 version of the file(s), but you are not obligated to do so.  If you
 do not wish to do so, delete this exception statement from your
 version.  If you delete this exception statement from all source
 files in the program, then also delete it here.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: T-Rex
 This software is provided 'as-is', without any express
 or implied warranty. In no event will the authors be held
 liable for any damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for
 any purpose, including commercial applications, and to alter
 it and redistribute it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented;
 you must not claim that you wrote the original software.
 If you use this software in a product, an acknowledgment
 in the product documentation would be appreciated but
 is not required.
 .
 2. Altered source versions must be plainly marked as such,
 and must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any
 source distribution.

Files: debian/*
Copyright: 2020-2023 Gürkan Myczko <tar@debian.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
